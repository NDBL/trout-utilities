# trout-utilities #

This repository contains various utility scripts for running trout-related workflows.  

These scripts are NOT error free or production.  They are intended as only a guide or useful resource.  

The directories correspond to the uses of the scripts.