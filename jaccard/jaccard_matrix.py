#!/usr/bin/python

import subprocess
import sys
import glob

PATH = '/afs/crc.nd.edu/group/NDBL/wmarkley/trout/util/jaccard/jaccard.py'
datadir    = sys.argv[1]
jellydumps = glob.glob(datadir+'*.jf');

distance  = {}

for jf1 in jellydumps:
	for jf2 in jellydumps:
		if jf1==jf2:
			if not jf1 in distance:
				distance[jf1] = {}
			distance[jf1][jf1] = float(0)
			continue

		if jf1 < jf2:
			fileA = jf1
			fileB = jf2
		else:
			fileA = jf2
			fileB = jf1

		if fileA in distance and fileB in distance[fileA]:
			continue
		elif not fileA in distance:
			distance[fileA] = {}

		command = PATH+' '+fileA+' '+fileB
		output = subprocess.check_output(command.split())
		output = output.split()[2]
		distance[fileA][fileB] = 1-float(output) # jaccard distance, not similarity


for fileA in sorted(distance):
	for fileB in sorted(distance[fileA]):
		outF1 = fileA.split('/')[-1]
		outF1 = outF1.split('.')[0]
		outF1 = outF1 + '.F.fq'

		outF2 = fileB.split('/')[-1]
                outF2 = outF2.split('.')[0]
                outF2 = outF2 + '.F.fq'

		print outF1+' '+outF2+' '+str(distance[fileA][fileB])
