#!/usr/bin/python

import os
import sys
import glob

datadir   = sys.argv[1]
#outputdir = sys.argv[2]
#fastqs    = glob.glob(datadir+'*.fq');
outputdir = '/afs/crc.nd.edu/group/NDBL/wmarkley/trout/results/creek/jellyfish/'
fastqs    = glob.glob(datadir+'*Actino16s.F.fq');

for fastq in fastqs:
	outputFile = fastq.split('/')[-1]
	outputFile = outputdir + outputFile.split('.F')[0]
	os.system('jellyfish count -C -m 19 -c 3 -s 10000000 -t 16 -o '+outputFile+'.jfdata '+fastq)
	os.system('jellyfish dump -c -L 150 '+outputFile+'.jfdata > '+outputFile+'.jf')
	os.system('rm '+outputFile+'.jfdata')
