#!/usr/bin/python

import sys
import sets

#requires two jellyfish dump files
#use the following command to convert a jellyfish database to a dump file
#    jellyfish dump -c jellyDatabaseFile

### Check arguments
if len(sys.argv) is not 3:
	print 'usage:	jaccard.py <jellyfish_dump> <jellyfish_dump>'
	exit(1);

dumpfile1 = open(sys.argv[1],'r')
dumpfile2 = open(sys.argv[2],'r')

set1 = set()
set2 = set()

for line in dumpfile1:
	set1.add(line.split()[0])

for line in dumpfile2:
	set2.add(line.split()[0])

intersection = float(len(set1.intersection(set2)))
union        = float(len(set1.union(set2)))
jaccard      = float(intersection/union)

print 'Jaccard Similiarity: '+str(jaccard)
