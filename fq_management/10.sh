mkdir -p a_new_directory
for f in *16s.F.fq; do
    file_size=$(wc -l < "$f")
    percent_size_as_float=$(echo "$file_size*.1" | bc)
    float_to_int=$(printf %.0f "$percent_size_as_float")
    grab_twenty=$(head -c "$float_to_int" "$f")
    new_fn=$(printf "%s_10" "$f") # new name file1_20
    printf "$grab_twenty" > a_new_directory/$new_fn
done
