#!/afs/crc.nd.edu/group/NDBL/wmarkley/software/bin/Rscript

#--------------------------------
# Clustering Functions
#--------------------------------

# colors a htclus (hc) by "K" a number of clusters
dendColorOnK <- function(hc, kclust) {
  labelColors = palette(rainbow(kclust))
  clusMember = cutree(hc, kclust)
  colClus <- function(n) {
    if (is.leaf(n)) {
      a <- attributes(n)
      labCol <- labelColors[clusMember[which(names(clusMember) == a$label)]]
      attr(n, "nodePar") <- c(a$nodePar, lab.col = labCol)
    }
    n
  }
  hcd = as.dendrogram(hc)
  clusDendro = dendrapply(hcd, colClus)
  return(clusDendro)
}

# returns a dendogram, sampInfo is a matrix of info, names of leaves are nameCol, clusterNum is clusCol
dendColorOnClus <- function(hc, sampInfo, nameCol, clusCol) {
  numClus = length(unique(sampInfo[[clusCol]]))
  #labelColors = palette(rainbow(numClus))
  labelColors = colorRampPalette(c("blue", "green"))(numClus)
  colLab <- function(n) {
    if (is.leaf(n)) {
      a <- attributes(n)
      # This removes the .fasta and then looks up the column in "t"
      #haystack = gsub("\\..*","",a$label)
      haystack = a$label
      indicies <- haystack == sampInfo[nameCol]
      clusNum = sampInfo[clusCol][indicies]
      labCol <- labelColors[as.integer(clusNum)]
      attr(n, "nodePar") <- c(a$nodePar, lab.col = labCol)
    }
    n
  }
  hcd = as.dendrogram(hc)
  clusDendro = dendrapply(hcd, colLab)
  return(clusDendro)
}

mentalOnClus <- function(distClus){
  mantelDist = list()
  t = 1
  for( i in 1:length(distClus)){
    distmat1 <- dist(do.call(rbind, distClus[i]))
    for( n in 1:length(distClus)){
      distmat2 <- dist(do.call(rbind, distClus[n]))
      manteltest <- mantel.rtest(distmat1, distmat2, nrepet = 9999)
      res <- c(i,n,manteltest$obs)
      mantelDist[[t]] <- res
      t = t + 1
    }
  }
  return(mantelDist)
}

nameDistToMatrix <- function(x){
  x.names <- sort(unique(c(x[[1]], x[[2]])))
  x.dist <- matrix(0, length(x.names), length(x.names))
  dimnames(x.dist) <- list(x.names, x.names)
  x.ind <- rbind(cbind(match(x[[1]], x.names), match(x[[2]], x.names)), cbind(match(x[[2]], x.names), match(x[[1]], x.names)))
  x.dist[x.ind] <- rep(x[[3]], 2)
  return(x.dist)
}

distByClus <- function(x, sampInfo, nameCol, clusCol, nameMod=""){
  plateInfo <- split(sampInfo, sampInfo[clusCol])
  distClus <- list()
  for( i in 1:length(plateInfo)){
    clus <- as.data.frame(plateInfo[i])
    dist <- x
    indicies <- unique(clus[,nameCol])
    indicies <- unlist( lapply(indicies, function(g) paste(g, nameMod, sep="")) )
    dist <- dist[dist[[1]] %in% indicies & dist[[2]] %in% indicies,]
    distClus[[i]] <- dist
  }
  return(distClus)
}

x <- read.table("x", as.is=TRUE)
t <- read.csv("t", as.is=TRUE)
x.dist <- nameDistToMatrix(x)
hc = hclust(dist(x.dist))
cutdendo = dendColorOnClus(hc,t,1,2)
plot(cutdendo, ylab="Distance", main = "Clustering Coloring")

