#!/afs/crc.nd.edu/group/NDBL/wmarkley/software/bin/Rscript

# distance matrix should be titled "input" and be in the CWD
x <- read.table("input", as.is=TRUE)

# right now x is mash results, this will be changed over to a simple jaccard distance, here we are building the distance table
x.names <- sort(unique(c(x[[1]], x[[2]])))
x.dist <- matrix(0, length(x.names), length(x.names))
dimnames(x.dist) <- list(x.names, x.names)
x.ind <- rbind(cbind(match(x[[1]], x.names), match(x[[2]], x.names)), cbind(match(x[[2]], x.names), match(x[[1]], x.names)))
x.dist[x.ind] <- rep(x[[3]], 2)

# prepare hierarchical cluster
hc = hclust(dist(x.dist))
hcd = as.dendrogram(hc)

pdf(file="output.pdf",width=100,height=50)
par(cex=1.25,font=1)
op = par(bg = "#DDE3CA")
plot(hcd,col="#487AA1", col.main="#45ADA8", col.lab="#7C8071", col.axis="#F38630", main="samples", sub="", xlab="", ylab="Mash D" )
dev.off()

