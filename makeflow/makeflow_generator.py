#!/usr/bin/python

import os
import sys
import glob

# run from trout directory, such as:
# ./util/makeflow_gen.py

executable = "./trout"

## INPUT FILES ##
# if input FASTQs are in a specific directory
files = glob.glob("*.fq")

# if input FASTQs are listed (with full path) in a text file
#files = open('data/CostalSEES/LCOI.fq.list', 'r');

# if markers are contained in one file
markers = "act.lsh"

# if multiple marker files exist
#markers = glob.glob("*.lsh")
# also requires logic in 'for' loop to select which marker file goes with which trout run
print ""

## Generate makeflow file ##

for file in files:
	output = os.path.splitext( os.path.basename(file) )[0]
	output += "-sketch.txt"
	
	## select correct marker file if multiple
	
	print output+": "+executable+" "+file.rstrip()+' '+LSH
	print "\t"+executable+" "+file.rstrip()+' '+markers+' '+output
	print ""
