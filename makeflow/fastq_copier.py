#!/usr/bin/python

import os
import sys
import glob

# run from trout directory, such as:
# ./util/makeflow_gen.py <fastqs.list>

PATH = "/tmp/wmarkleyMakeflow/"

#files = glob.glob(PATH+"*.fq")
files = open(sys.argv[1], 'r');

for file in files:
	os.system("cp "+file.rstrip()+" "+PATH+os.path.basename(file.rstrip()))
