#!/usr/bin/python

import subprocess
import sys
import glob

PATH = 'sh'

datadir = sys.argv[1]
fastqs = glob.glob(datadir+'*.fq');

print "Total\t\t#>1\t\tFilename"
outputFile = datadir+'mer_count.jf'

for fastq in fastqs:
	command = 'jellyfish count -C -m 19 -c 3 -s 10000000 -t 16 -o '+outputFile+' '+fastq
	output = subprocess.check_output(command.split())
	command = PATH+'/total.sh '+outputFile
	total_mers = subprocess.check_output(command.split())
	command = PATH+'/once.sh '+outputFile
	found_once = subprocess.check_output(command.split())

	total_mers = int(total_mers)	
	result = total_mers-int(found_once)

	print str(total_mers)+"\t\t"+str(result)+"\t\t"+fastq
