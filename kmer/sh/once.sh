#!/bin/bash


jellyfish dump -c $1 | cut -d ' ' -f 2 | awk 'length($0) == 1' | grep 1 | wc -l
