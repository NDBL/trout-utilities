#!/afs/nd.edu/user15/pbui/pub/anaconda-2.3.0/bin/python

import sys
import glob
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

### Read in sketch data from files

data = []
results = []

sketches = glob.glob(sys.argv[1]+"*sketch.txt");

for sketch in sketches:
	f = open(sketch,'r')
	i = 0
	count = 0
	for line in f:
		i += 1
		data = line.split(':')[1].split()
		for d in data:
			if d=="1":
				count += 1
	if i is not 1:
		print "error: multiple sketches written to file "+sketch
		exit(1)
	results.append(count)
	f.close()

'''
if count in results:
		results[count] += 1
	else:
		results[count] = 1
'''

print results
totals, bin_edges = numpy.histogram(results, 40, range=[0,400000])
plt.bar(bin_edges[:-1], totals, width = 1)
plt.xlim(min(bin_edges), max(bin_edges))
plt.savefig('histogram.png')
#for i in range(0,len(totals)):
#	print str(bin_edges[i+1])+'<'+str(totals[i])


'''
for i in range(0,400001):
	if i in results:
		print str(i)+','+str(results[i])
	else:
		print str(i)+','+str(0)
'''
